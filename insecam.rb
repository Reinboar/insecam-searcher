require 'mechanize'

class Camera

  attr_accessor :ip, :country, :country_code,:region, :city, :latitude, :longitude, :zipcode, :timezone, :manufacturer 

  def initialize ip, country, country_code, region, city, latitude, longitude, zipcode, timezone, manufacturer
    @ip = ip
    @country = country
    @country_code = country_code
    @region = region
    @city = city
    @latitude = latitude
    @longitude = longitude
    @zipcode = zipcode
    @timezone = timezone
    @manufacturer = manufacturer
  end

  def to_h
    {ip: @ip, country: @country, country_code: @country_code, region: @region, city: @city, latitude: @latitude, longitude: @longitude, zipcode: @zipcode, timezone: @timezone, manufacturer: @manufacturer}
  end

end

class InsecamSite
  
  attr_accessor :base_url

  def initialize base_url="http://www.insecam.org"
    @base_url = base_url
  end

  def by_country_code country_code, page=1
     _search_site(:country_code , country_code, page)
  end

  def by_manufacturer manufacturer, page=1
    _search_site(:manufacturer , manufacturer, page)
  end

  def by_area_type area_type, page=1
     _search_site(:area , area_type, page)
  end

  def all page=1
    _search_site(:all ,nil, page)
  end

  private

  def _search_site type=:country_code, subject=nil, page
    agent = Mechanize.new
    case type
    when :country_code
      relative_url = "/en/bycountry/#{subject}/?page=#{page}"
    when :manufacturer
      relative_url = "/en/bytype/#{subject}/?page=#{page}"
    when :area
      relative_url = "/en/bytag/#{subject}/?page=#{page}"
    when :all
      relative_url = "/en/byrating/?page=#{page}"
    end
    _create_array_from_camera_list(agent.get(@base_url + relative_url))
  end

  def _get_camera_info_from_camera_page page

	  ip = page.css('div > div > main > article').css('div')[2].css('a')[0]["href"] 
	  page = page.css('.row')
	  country = page[0].at_css('.detailsdesc').content.gsub("\n","").scan(/^([\-\s\w]+)[\,\.]/)[0][0]
          country_code = page[1].at_css('.detailsdesc').content.gsub("\n","")
          region = page[2].at_css('.detailsdesc').content.gsub("\n","")
          city = page[3].at_css('.detailsdesc').content.gsub("\n","").scan(/^([\-\s\w]+)[\,\.]/)[0][0] 
	  lat = page[4].at_css('.detailsdesc').content.gsub("\n","")
	  long = page[5].at_css('.detailsdesc').content.gsub("\n","")
	  zipcode = page[6].at_css('.detailsdesc').content.gsub("\n","")
	  timezone = page[7].at_css('.detailsdesc').content.gsub("\n","")
	  manu = page[8].at_css('.detailsdesc').content.gsub("\n","")
	  Camera.new ip, country, country_code, region, city, lat, long, zipcode, timezone, manu 
  end

  def _create_array_from_camera_list page
    agent = Mechanize.new
    {:cameras => page.css('.thumbnail-item__wrap').map do |cam|
      _get_camera_info_from_camera_page agent.get(@base_url + cam["href"])
    end, :num_pages => page.css('ul.pagination > script')[0].content.scan(/pagenavigator\(\"\?page=\"\, (\d+).+\)/)[0][0].to_i}
  end

end
