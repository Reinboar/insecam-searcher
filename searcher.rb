#!/usr/bin/ruby
require "./insecam.rb"
require "optparse"

#search_hash can contain symbols corresponding with the Camera class's attributes
def search search_hash, verbose, file
  site = InsecamSite.new
  search_method = nil
  if search_hash[:area] then
    search_method = lambda { |page| site.by_area_type search_hash[:area], page }
  elsif search_hash[:country_code] then
    search_method = lambda { |page| site.by_country_code search_hash[:country_code], page }
  elsif search_hash[:manufacturer] then
    search_method = lambda { |page| site.by_manufacturer search_hash[:manufacturer], page }
  else
    search_method = lambda { |page| site.all page } 
  end
  pages = search_method.call(1)[:num_pages]
  pages.times do |i|
    puts "Page #{i}/#{pages}" if verbose
    search_method.call(i+1)[:cameras].each do |cam|
      file.puts cam.ip if (((search_hash.select {|k,v| k!=:area}).to_a) - cam.to_h.to_a).to_h.empty?
    end
  end
end

file = $stdout
ver = nil
options = {}
OptionParser.new { |opts|
  opts.banner = "Usage: searcher.rb <options>"

  opts.on("-d", "--country-code countrycode", "Specify country code") do |cc|
    options[:country_code] = cc
  end
  opts.on("-m", "--manufacturer manufacturer", "Specify camera manufacturer") do |m|
    options[:manufacturer] = m
  end
  opts.on("-c", "--city city", "Specify city") do |c|
    options[:city] = c
  end
  opts.on("-a", "--area area", "Specify area") do |a|
    options[:area] = a
  end
  opts.on("-r", "--region region", "Specify region") do |r|
    options[:region] = r
  end
  opts.on("-v", "--verbose", "Output verbose information") do |v|
    ver = true
  end
  opts.on("-f", "--file file", "Specify file to output IPs to. If unspecified, defaults to stdout") do |f|
    file = File.open(f, "w")
  end
}.parse!

search(options, ver, file)
file.close
